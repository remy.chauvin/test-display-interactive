# Test: Interactive display

## Get started

To run the project, you'll need to install some utilities.

### Requirements

 - Poetry (https://python-poetry.org/docs/ )
 - Python >= 3.11

I recommend pyenv for easy installation of different python versions
https://github.com/pyenv/pyenv?tab=readme-ov-file#installation

### Project installation / configuration

Once the project has been cloned onto your computer.
Run this command in the project's root directory to install dependencies:

    poetry install

All dependencies should now be installed.

You need to set the environment variables

The `.env.example` file contains an example of the required environment variables.
You can rename the file to `.env`, and the environment variables will be automatically set when the project is launched, based on the values in this file.

### Usage

This command displays the options supported by the utility.

    poetry run python -m sync --help
    
| Arg   | Description |
|--|--|
| --customers-path | Location of customer CSV file on your computer  |
| --purchases-path | Location of purchases CSV file on your computer  |

Example CSV files are provided with the project, you can run this command from project's root directory to start synchronization with these data.

    poetry run python -m sync --customers-path ./files/customers.csv --purchases-path ./files/purchases.csv



### Run test suite

A very basic test suite is provided with the project, and you can use this command to launch it.

```
poetry run pytest
```

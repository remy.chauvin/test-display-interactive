from unittest.mock import patch
from sync.sync import prepare_data, sync_purchases

from .test_data import invalid_purchase, purchase, customers


def test_should_ignore_purchase_when_customer_doest_not_exist():
    purchases = prepare_data(customers=customers, purchases=[invalid_purchase])

    assert len(purchases) == 0


@patch("sync.sync.httpx.put")
def test_should_make_api_call_to_sync_purchases(httpx_mock):
    nb = sync_purchases(customers=customers, purchases=[purchase])
    assert nb == 1
    httpx_mock.assert_called_once()

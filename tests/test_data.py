customers = [
    {
        "customer_id": "1",
        "title": "2",
        "lastname": "Norris",
        "firstname": "Chuck",
        "postal_code": "83600",
        "city": "Fréjus",
        "email": "chuck@norris.com",
    }
]

purchase = {
    "purchase_identifier": "1/01",
    "customer_id": "1",
    "product_id": "1221",
    "quantity": "1",
    "price": "10",
    "currency": "EUR",
    "date": "2017-12-31",
}

invalid_purchase = {
    "purchase_identifier": "2/01",
    "customer_id": "2",
    "product_id": "1221",
    "quantity": "1",
    "price": "10",
    "currency": "EUR",
    "date": "2017-12-31",
}

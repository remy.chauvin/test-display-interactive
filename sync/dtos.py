from datetime import date
from typing import List
from pydantic import BaseModel


class Purchase(BaseModel):
    product_id: str
    price: int
    currency: str
    quantity: int
    purchased_at: date


class PurchaseGroup(BaseModel):
    salutation: str
    last_name: str
    first_name: str
    email: str
    purchases: List[Purchase]

from collections import defaultdict
from datetime import datetime
from typing import Dict, List
from pydantic_core import to_json
import httpx

from sync.dtos import Purchase, PurchaseGroup
import logging
from urllib.parse import urljoin
from sync.settings import settings


logger = logging.getLogger(__name__)


def prepare_data(customers: List[Dict], purchases: List[Dict]) -> List[PurchaseGroup]:
    grouped_purchases = defaultdict(list)
    for p in purchases:
        grouped_purchases[p["customer_id"]].append(p)

    data = [
        PurchaseGroup(
            salutation="Ms" if customer["title"] == "1" else "Mr",
            last_name=customer.get("lastname", ""),
            first_name=customer.get("firstname", ""),
            email=customer.get("email", ""),
            purchases=[
                Purchase(
                    product_id=purchase["product_id"],
                    price=int(purchase["price"]),
                    currency=purchase["currency"],
                    quantity=int(purchase["quantity"]),
                    purchased_at=datetime.strptime(purchase["date"], "%Y-%m-%d").date(),
                )
                for purchase in grouped_purchases[customer["customer_id"]]
            ],
        )
        for customer in customers
        if customer.get("customer_id") in grouped_purchases
    ]

    return data


def sync_purchases(customers: List[Dict], purchases: List[Dict]) -> int:
    purchases_group = prepare_data(customers=customers, purchases=purchases)
    nb_orders = len(purchases_group)

    if not nb_orders:
        return 0

    data = to_json(purchases_group)
    try:
        endpoint = urljoin(settings.API_BASE_URL, "/v1/customers")
        r = httpx.put(
            endpoint,
            content=data,
            headers={"content-type": "application/json"},
        )
        r.raise_for_status()
    except httpx.HTTPError as e:
        logger.error(
            f"Cannot sync purchases with remote server using base url {endpoint}"
        )
        raise e

    return nb_orders

import typer
from pathlib import Path
from typing_extensions import Annotated

from sync.reader import read_csv
from sync.sync import sync_purchases
import logging
from rich import print

logger = logging.getLogger(__name__)


app = typer.Typer(pretty_exceptions_enable=False)


@app.command()
def sync(
    customers_path: Annotated[
        Path,
        typer.Option(
            exists=True,
            file_okay=True,
            dir_okay=False,
            writable=False,
            readable=True,
            resolve_path=True,
        ),
    ],
    purchases_path: Annotated[
        Path,
        typer.Option(
            exists=True,
            file_okay=True,
            dir_okay=False,
            writable=False,
            readable=True,
            resolve_path=True,
        ),
    ],
):
    logging.debug("Read customer CSV file")
    customers = read_csv(filepath=customers_path)
    logger.debug("Read purchases CSV file")
    purchases = read_csv(filepath=purchases_path)
    try:
        nb_synced = sync_purchases(customers=customers, purchases=purchases)
        print(
            f"[bold green]Orders from {nb_synced} customer(s) have been successfully synchronized.[/bold green]"
        )

    except Exception as e:
        print("[bold red]Error occured during sync[/bold red]")
        raise e


if __name__ == "__main__":
    app()

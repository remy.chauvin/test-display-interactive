from pathlib import Path
import csv
from typing import Dict, List


def read_csv(filepath: Path) -> List[Dict]:
    entries = []
    with open(filepath, newline="") as customer_file:
        csv_reader = csv.DictReader(customer_file, delimiter=";")
        for line_number, row in enumerate(csv_reader):
            if line_number == 0:
                pass

            entries.append(row)

    return entries
